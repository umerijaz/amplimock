#!/usr/bin/python
import os,sys,os.path
import time,datetime
import subprocess
from optparse import OptionParser
from optparse import Option, OptionValueError
import csv
import re
import pylab
from Bio import SeqIO
from Bio.SeqUtils import GC
import math

import multiprocessing
from textwrap import dedent
from itertools import izip_longest


#Ref:http://stackoverflow.com/questions/5319922/python-check-if-word-is-in-a-string
def findWholeWord(w):
    #return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search
    return re.compile(r'({0})'.format(w), flags=re.IGNORECASE).search	

#Ref:http://www.ngcrawford.com/2012/03/29/python-multiprocessing-large-files/
def process_chunk(d):
	"""Replace this with your own function that processes data one line at a time"""
 	d = d.strip()
	return d
 
def grouper(n, iterable, padvalue=None):
	"""grouper(3, 'abcdefg', 'x') -->('a','b','c'), ('d','e','f'), ('g','x','x')"""
	return izip_longest(*[iter(iterable)]*n, fillvalue=padvalue)

def run_prog(prog):
#    p = subprocess.Popen(prog, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    p = subprocess.Popen(prog, shell=True, stdout=None, stderr=None)
#    for line in iter(p.stdout.readline, b''):
#       print(">>> " + line.rstrip())
    retval = p.wait()

#Ref:http://preshing.com/20110924/timing-your-code-using-pythons-with-statement
class Timer:    
    def __enter__(self):
        self.start = time.clock()
        return self

    def __exit__(self, *args):
        self.end = time.clock()
        self.interval = self.end - self.start

#Ref:http://stackoverflow.com/questions/4109436/processing-multiple-values-for-one-single-option-using-getopt-optparse
class MultipleOption(Option):
    ACTIONS = Option.ACTIONS + ("extend",)
    STORE_ACTIONS = Option.STORE_ACTIONS + ("extend",)
    TYPED_ACTIONS = Option.TYPED_ACTIONS + ("extend",)
    ALWAYS_TYPED_ACTIONS = Option.ALWAYS_TYPED_ACTIONS + ("extend",)

    def take_action(self, action, dest, opt, value, values, parser):
        if action == "extend":
            values.ensure_value(dest, []).append(value)
        else:
            Option.take_action(self, action, dest, opt, value, values, parser)

def write_dict_file_csv(mydict,filename):
    writer = csv.writer(open(filename, 'wb'),delimiter=',')
    for key, value in mydict.items():
        writer.writerow([key,value])

def read_dict_file_csv(filename):
    reader = csv.reader(open(filename, 'rb'),delimiter=',')
    mydict = dict(x for x in reader)
    return mydict

def fasta_total_records(fasta_file):
    cmd='grep -q \"size=\" '+fasta_file+ ' && echo \"1\" || echo \"0\"'
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    record=None
    for line in iter(p.stdout.readline, b''):
        record=line.rstrip()
    retval=p.wait()  
    if record=="1":
	cmd='awk -F\"size=\" \'/>/{gsub(\";$\",\"\",$2);sum+=$2} END {print sum}\' '+fasta_file
    else:
        cmd='grep -c \">\" '+fasta_file
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    record=None
    for line in iter(p.stdout.readline, b''):
        record=line.rstrip()
    retval=p.wait()
    return record


def gen_table(balanced_usearch_file,balanced_total_records,unbalanced_usearch_file,unbalanced_total_records,mapping_dict,proportions_dict,balanced_proportion):

    freq_balanced_dict={}
    with open(balanced_usearch_file) as f:
                for line in f:
                        line=line.rstrip()
                        d=line.split('\t')
                        size_match=re.findall(r';size=(\w.*?);',d[0])
                        res=mapping_dict.get(d[1], None)
                        if res==None:
                                if size_match:
                                        freq_balanced_dict[d[1]]=int(size_match[0])+freq_balanced_dict.get(d[1],0)
                                else:
                                        freq_balanced_dict[d[1]]=1+freq_balanced_dict.get(d[1],0)
                        else:
                                if size_match:
                                        freq_balanced_dict[res]=int(size_match[0])+freq_balanced_dict.get(res,0)
                                else:
                                        freq_balanced_dict[res]=1+freq_balanced_dict.get(res,0)

    for key in freq_balanced_dict.keys():
        if freq_balanced_dict[key]>0:
                freq_balanced_dict[key]=float(freq_balanced_dict[key])/float(balanced_total_records)



    freq_unbalanced_dict={}
    with open(unbalanced_usearch_file) as f:
                for line in f:
                        line=line.rstrip()
                        d=line.split('\t')
                        size_match=re.findall(r';size=(\w.*?);',d[0])
                        res=mapping_dict.get(d[1], None)
                        if res==None:
                                if size_match:
                                        freq_unbalanced_dict[d[1]]=int(size_match[0])+freq_unbalanced_dict.get(d[1],0)
                                else:
                                        freq_unbalanced_dict[d[1]]=1+freq_unbalanced_dict.get(d[1],0)
                        else:
                                if size_match:
                                        freq_unbalanced_dict[res]=int(size_match[0])+freq_unbalanced_dict.get(res,0)
                                else:
                                        freq_unbalanced_dict[res]=1+freq_unbalanced_dict.get(res,0)

    for key in freq_unbalanced_dict.keys():
        if freq_unbalanced_dict[key]>0:
                freq_unbalanced_dict[key]=float(freq_unbalanced_dict[key])/float(unbalanced_total_records)
    print "Species,true_balanced,estim_balanced,true_unbalanced,estim_unbalanced"	
    s = set( val for dic in mapping_dict for val in mapping_dict.values() )
    for key in s:
        if not(freq_balanced_dict.get(key,None)==None or freq_unbalanced_dict.get(key,None)==None or proportions_dict.get(key,None)==None or proportions_dict.get(key,None)==""):
		print key+","+str(balanced_proportion)+","+str(freq_balanced_dict.get(key,None))+","+str(proportions_dict.get(key,None))+","+str(freq_unbalanced_dict.get(key,None))

def main():
    parser = OptionParser(option_class=MultipleOption,usage="usage: %prog [options] filename",
                          version="%prog 0.1")
    parser.add_option("-l","--list_file",
                       action="store",
                       default=None,
                       dest="list_file",
                       help="list file that contains the mapping from multiple 16S copies to genome")
    parser.add_option("-p","--proportions_file",
                       action="store",
                       default=None,
                       dest="proportions_file",
                       help="proportions files for unbalanced mock")
    parser.add_option("-b","--fasta_file_balanced",
                       action="store",
                       default=None,
                       dest="fasta_file_balanced",
                       help="fasta_file_balanced")
    parser.add_option("-c","--usearch_file_balanced",
                       action="store",
                       default=None,
                       dest="usearch_file_balanced",
                       help="usearch_file_balanced")

    parser.add_option("-u","--fasta_file_unbalanced",
                       action="store",
                       default=None,
                       dest="fasta_file_unbalanced",
                       help="fasta_file_unbalanced")
    parser.add_option("-v","--usearch_file_unbalanced",
                       action="store",
                       default=None,
                       dest="usearch_file_unbalanced",
                       help="usearch_file_unbalanced")

    if len(sys.argv) == 1:
        parser.parse_args(['--help'])

    options, args = parser.parse_args()

    mapping_dict=read_dict_file_csv(options.list_file)
    proportions_dict=read_dict_file_csv(options.proportions_file)	

    gen_table(options.usearch_file_balanced,fasta_total_records(options.fasta_file_balanced),options.usearch_file_unbalanced,fasta_total_records(options.fasta_file_unbalanced),mapping_dict,proportions_dict,0.01694915)	

if __name__ == '__main__':
    main()



