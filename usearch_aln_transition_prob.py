#!/usr/bin/python 
# ***************************************************************
# Name:      usearch_aln_transition_prob.py
# Purpose:   This script calculate transition probabilities for alignment files
# 	     generated from usearch (*.aln)
# 	     Format of a typical file is as follows:
#
# <TEST.aln>
# usearch -usearch_global nested_pcr_ATCACG_L001_R1_001_trim.fasta -db /home/uzi/TSB_AMPLICONS_REFERENCE/Mercier_16S_reference_251013C.fasta -id 0.95 -alnout nested_pcr_ATCACG_L001_R1_001_trim.aln -strand plus -userout nested_pcr_ATCACG_L001_R1_001_trim.ualn -userfields query+target+id+alnlen+mism+opens+qlo+qhi+tlo+thi+evalue+bits -matched nested_pcr_ATCACG_L001_R1_001_trim_matched.fasta -notmatched nested_pcr_ATCACG_L001_R1_001_trim_notmatched.fasta -threads 5
# usearch_i86linux32 v6.0.307, 4.0Gb RAM (529Gb total), 64 cores
# 
# Query >MISEQ:14:000000000-A1FNW:1:1101:15022:1463 1:N:0:ATCACG
#  %Id   TLen  Target
#  99%   1144  Chlorobiumtepidum_TLS_hap1
# 
#  Query  150nt >MISEQ:14:000000000-A1FNW:1:1101:15022:1463 1:N:0:ATCACG
# Target 1144nt >Chlorobiumtepidum_TLS_hap1
# 
# Qry    1 + GTGCCAGCAGCCGCGGTGATACAGGGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGT 64
#            ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# Tgt  259 + GTGCCAGCAGCCGCGGTGATACAGGGGTGGCAAGCGTTGTCCGGATTTACTGGGTGTAAAGGGT 322
# 
# Qry   65 + GCGCAGGCGGATCGATAAGTCGGGGGTTAAATCCATGTGCTTAACACATGTACGGCTTCCGATA 128
#            |||||||||||||||||||||||||||||||||||||||||||||||||| |||||||||||||
# Tgt  323 + GCGCAGGCGGATCGATAAGTCGGGGGTTAAATCCATGTGCTTAACACATGCACGGCTTCCGATA 386
# 
# Qry  129 + CTGTTGATCTAGAGTCTCGAAG 150
#            ||||||||||||||||||||||
# Tgt  387 + CTGTTGATCTAGAGTCTCGAAG 408
# 
# 150 cols, 149 ids (99.3%), 0 gaps (0.0%)
# 
# Query >MISEQ:14:000000000-A1FNW:1:1101:16372:1472 1:N:0:ATCACG
#  %Id   TLen  Target
#  99%   1542  Dickeya_dadantii_3937_chromosome_NC_014500.1_complete_genome_6
# 
#  Query  150nt >MISEQ:14:000000000-A1FNW:1:1101:16372:1472 1:N:0:ATCACG
# Target 1542nt >Dickeya_dadantii_3937_chromosome_NC_014500.1_complete_genome_6
# 
# Qry    1 + GTGCCAGCCGCCGCGGTAATACGGAGGGTGCAAGCGTTAATCGGAATGACTGGGCGTAAAGCGC 64
#            ||| |||| |||||||||||||||||||||||||||||||||||||||||||||||||||||||
# Tgt  515 + GTGTCAGCAGCCGCGGTAATACGGAGGGTGCAAGCGTTAATCGGAATGACTGGGCGTAAAGCGC 578
# 
# Qry   65 + ACGCAGGCGGTCTGTTAAGTTGGATGTGAAATCCCCGGGCTTAACCTGGGAACTGCATTCAAAA 128
#            ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# Tgt  579 + ACGCAGGCGGTCTGTTAAGTTGGATGTGAAATCCCCGGGCTTAACCTGGGAACTGCATTCAAAA 642
# 
# Qry  129 + CTGACAGGCTAGAGTCTCGTAG 150
#            ||||||||||||||||||||||
# Tgt  643 + CTGACAGGCTAGAGTCTCGTAG 664
# 
# 150 cols, 148 ids (98.7%), 0 gaps (0.0%)
# </TEST.aln>
#
# 	     Steps involved:
#            	(1): We first parse the file such that we populate four strings per record, i.e.,
# 		     query_name,reference_name, query_string, reference_string
# 		(2): For each record, multiline "Qry" and "Tgt" get concatentated together to construct query_string and reference_string, respectively
# 		(3): Once whole record is assembled, we use calculate_transition_counts() to calculate transition probabilities              
#
# Version:   0.1 
# Authors:   Umer Zeeshan Ijaz (Umer.Ijaz@glasgow.ac.uk)
#                 http://userweb.eng.gla.ac.uk/umer.ijaz
# Last Modified:   2014-03-10
# License:   Copyright (c) 2014 Computational Microbial Genomics Group, University of Glasgow, UK
#
#            This program is free software: you can redistribute it and/or modify
#            it under the terms of the GNU General Public License as published by
#            the Free Software Foundation, either version 3 of the License, or
#            (at your option) any later version.
#
#            This program is distributed in the hope that it will be useful,
#            but WITHOUT ANY WARRANTY; without even the implied warranty of
#            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#            GNU General Public License for more details.
#
#            You should have received a copy of the GNU General Public License
#            along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ************************************************************** 
import sys, getopt,os
import re

def usage():
	print 'Usage:'
	print '\tpython usearch_aln_transition_prob.py -i file.aln [options]\n'
	print '\t -k NUM --kmer_size NUM\tkmer size (Default:1)'
	print '\t -b \t Flag for bidirectional transitions (Default:OFF)'
	print '\t -c \t Flag for displaying counts instead of transition probablities (Default:OFF)' 

transition_table={}

def calculate_transition_counts(query,reference,kmer_size,bidirectional):
        for i in range(len(query)-(kmer_size-1)):
                 lk=query[i:i+kmer_size].upper()
                 rk=reference[i:i+kmer_size].upper()   
		 if bidirectional==1:
                 	if transition_table.get(lk+':'+rk,None)==None:
		 		if transition_table.get(rk+':'+lk,None)==None:
					transition_table[rk+':'+lk]= 1
				else:
					transition_table[rk+':'+lk]=transition_table[rk+':'+lk]+1
		 	else:
				transition_table[lk+':'+rk]=transition_table[lk+':'+rk]+1
		 else:
			if transition_table.get(lk+':'+rk,None)==None:
				transition_table[lk+':'+rk]= 1
			else:
				transition_table[lk+':'+rk]=transition_table[lk+':'+rk]+1	 

def main(argv):    
	usearch_file = ''
	outputfolder=''
	bidirectional=0
	kmer_size=1
	transition_count=0        
	try:       
		opts, args =getopt.getopt(argv,"hbck:i:",["usearch_aln_file=","kmer_size="])    
	except getopt.GetoptError:       
		usage()       
		sys.exit(2)   
	for opt, arg in opts:       
		if opt == '-h':          
			usage()          
			sys.exit()       
		elif opt in ("-i", "--usearch_aln_file"):          
			usearch_file = arg
                elif opt in ("-k", "--kmer_size"):          
                        kmer_size = arg
		elif opt == '-b':
			bidirectional=1
       		elif opt == '-c':
			transition_count=1	

	if usearch_file=='':
		usage()
		sys.exit(2)
	line=''
	ins=open(usearch_file,"r")
	while 1:
		line=ins.readline()
		#if EOF then break
		if not line:
			break
		line=line.rstrip()
		matchObj=re.match(r'Query >(.*)',line,re.M|re.I)
		query_name=''
		reference_name=''
		query_string=''
		reference_string=''
		if matchObj:
			query_name=matchObj.group(1)
			while 1:
                        	pos=ins.tell()
                                line=ins.readline()
                                #if EOF then break
                                if not line:
                                	break
                                matchObj=re.match(r'Query >(.*)',line,re.M|re.I)
                                #next Query encountered, break and go back to previous position
                                if matchObj:
                                       	ins.seek(pos)
                                        break
                                line=line.rstrip()
				matchObj=re.match(r'Target (.*?) >(.*)',line,re.M|re.I)
				if matchObj:
					reference_name=matchObj.group(2)
					while 1:
						pos=ins.tell()
						line=ins.readline()
						#if EOF then break
						if not line:
                        				break
						matchObj=re.match(r'Query >(.*)',line,re.M|re.I)
						#next Query encountered, break and go back to previous position
						if matchObj:
							ins.seek(pos)
							break
						line=line.rstrip()
						matchObj=re.match(r'Qry\s+\d+\s+[\+\-]\s+(.*)\s+\d+',line,re.M|re.I)
						if matchObj:
							query_string=query_string+matchObj.group(1)
							while 1:
								line=ins.readline()
								line=line.rstrip()
								matchObj=re.match(r'Tgt\s+\d+\s+[\+\-]\s+(.*)\s+\d+',line,re.M|re.I)
								if matchObj:
									reference_string=reference_string+matchObj.group(1)
									break
		#Extracted Record ####################################

		#Record is stored in query_name,query_string,reference_name, and reference_string 
		if (len(query_string)!=0 and len(reference_string)!=0):
			calculate_transition_counts(query_string,reference_string,int(kmer_size),bidirectional)


		#/Extracted Record ###################################		
	ins.close()
	total_count=0

	if transition_count==0:
		for key in transition_table.keys():
			total_count=total_count+int(transition_table[key])
        for key in transition_table.keys():
        	if transition_count==0:
			print key+","+str(float(transition_table[key])/total_count)
		else:
			print key+","+str(transition_table[key])

if __name__ == "__main__":    
	main(sys.argv[1:])

